<?php
include('connectDB.php');

$result = $conn -> query('SELECT id, firstname, lastname, email, message, date FROM form_message');
while ( $row = mysqli_fetch_array($result) ) {
    echo '<div class="item"> 
        <div class="item-id"><div>'.$row['id'].'</div></div>
        <div class="item-firstname"><div>'.$row['firstname'].'</div></div>
        <div class="item-lastname"><div>'.$row['lastname'].'</div></div>
        <div class="item-email"><div>'.$row['email'].'</div></div>
        <div class="item-message"><div>'.$row['message'].'</div></div>
        <a href="../view/view.php?id='.$row['id'].'" class="item-view"><div class="button">Wiew</div></a>
        <a href="../mysql/deleteMessage.php?id='.$row['id'].'" class="item-delete"><div class="button">Delete</div></a>
    </div>';
}
?>