<?php 
$serverName = 'localhost';
$userName = 'root';
$password = '';

$conn = new mysqli($serverName, $userName, $password);
if ( $conn -> connect_error ) {
    die ("Not conection" . $conn -> connect_error);
}

$db_selected = mysqli_select_db($conn, 'message');
if ( !$db_selected ) {
    $createDB = 'CREATE DATABASE IF NOT EXISTS message character set UTF8 collate utf8_general_ci';
    if ( mysqli_query($conn, $createDB) ) {
        mysqli_select_db($conn, 'message');
    } else {
        echo 'Error creating database: ' . mysqli_error($conn);
    }
}

$createTable = "CREATE TABLE IF NOT EXISTS form_message (
    id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(30) NOT NULL,
    email VARCHAR(50),
    message VARCHAR(255),
    date TIMESTAMP
)";
mysqli_query($conn, 'ALTER TABLE form_message AUTO_INCREMENT=1');
if ( mysqli_query($conn, $createTable) ) {

} else {
    echo 'Error creating Table: ' . mysqli_error($conn);
}

mysqli_close($conn);
?>