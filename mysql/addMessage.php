<?php 
include('connectDB.php');

if ( isset($_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['message']) ) {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $stmt = $conn -> prepare('INSERT INTO form_message (firstname, lastname, email, message) VALUES (?, ?, ?, ?)');
    $stmt -> bind_param('ssss', $first_name, $last_name, $email, $message);
    $stmt->execute();
}

header('Location: ../login/login.php');

mysqli_stmt_close($stmt);
mysqli_close($conn);
?>