$('.button').click(() => {
	let first_name = $('input[name="first_name"]').val();
	let last_name = $('input[name="last_name"]').val();
	let email = $('input[name="email"]').val();
	let message = $('textarea[name="message"]').val();
	if (first_name == '' || last_name == '' || email == '' || message == '') {
		$('.required').addClass('required-anim');
		event.preventDefault();
	}
});
