<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>View</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/Variables.css" />
    <link rel="stylesheet" type="text/css" href="../css/GlobalStyle.css"/>
    <link rel="stylesheet" type="text/css" href="../css/view.css" />
</head>
<body>
    <section class="section">
        <div class="items">
            <div class="titles">
                <div class="id">Id</div>
                <div class="first_name">First name</div>
                <div class="last_name">Last name</div>
                <div class="email">Email</div>
                <div class="message">Message</div>
                <div class="date">Date</div>
            </div>
            <?php 
                include('../mysql/getMessageItem.php');
            ?>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="../js/index.js"></script>
    <script type="text/javascript" src="../js/view.js"></script>
</body>
</html>