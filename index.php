<?php
require_once('mysql/createDB.php')
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Message</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/Variables.css" />
    <link rel="stylesheet" type="text/css" href="css/GlobalStyle.css"/>
    <link rel="stylesheet" type="text/css" href="css/FormMessage.css"/>
</head>
<body>
    <section class="section">
        <div class="container">
            <div class="FormMessage">
                <div class="FormMessage-image col-lg-6 col-12"><img src="https://colorlib.com/etc/cf/ContactFrom_v1/images/img-01.png"/></div>
                <form class="col-lg-6 col-12" action="/mysql/addMessage.php" method="post">
                    <h1 class="title">Get in touch</h1>
                    <input type="text" name="first_name" placeholder="First name"/>
                    <input type="text" name="last_name" placeholder="Last name"/>
                    <input type="email" name="email" placeholder="Email"/>
                    <textarea name="message" placeholder="Message"></textarea>
                    <div class="required"><span>This field should not be left blank.<span></div>
                    <button class="button" type="submit" >Send Message <i class="fas fa-hand-point-right"></i></button>
                </form>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/FormMessage.js"></script>
</body>
</html>